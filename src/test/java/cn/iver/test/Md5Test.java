package cn.iver.test;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Test;

import com.jfinal.kit.HashKit;

public class Md5Test {

	// 字符集不一样时，md5出来的值不一样（中文情况），请大家注意HashKit中默认使用了UTF-8
	@Test
	public void test() throws UnsupportedEncodingException {
		String data = "JFinal-bbs中文";

		System.out.println(DigestUtils.md5Hex(data.getBytes("GBK")));

		System.out.println(DigestUtils.md5Hex(data.getBytes("UTF-8")));

		System.out.println(HashKit.md5(data));
	}
}
